'use strict';

/**
 * paymentStats object stores the backend data as well as
 * contain utility methods.
 */
var paymentStats = {  

    url: 'http://localhost:3000/payments',

    callbackExample: function() {
        $.getJSON(this.url + "?_limit=20&_sort=amount&_order=DESC", function(data){

            if(!$('#payments-table').is(':visible')) {
                $('#payments-table').toggle('show');            
            } 

            paymentStats.populateTable(data, '#payments-table');
        });        
    },

    promiseExample: function() {

        $.getJSON(this.url + "?merchant=Ginger")

        .done(function(data) {               

            if(!$('#payments-table').is(':visible')) {
                $('#payments-table').toggle('show');
            }

            paymentStats.populateTable(data, '#payments-table');
        });

    },

    filterExample: function() {
        $.getJSON("http://localhost:3000/payments", function(data){

            if(!$('#payments-table-filter').is(':visible')) {
                $('#payments-table-filter').toggle('show');            
            }

            paymentStats.populateTable(data, '#payments-table-filter');
        
        });
    },

    showAddPaymentForm: function() {
        if(!$("form").is(':visible')) {
            $("#payment-container").hide();
            $("form").show();
            $("#form-response").hide();
            $("form").trigger('reset');
            $("form #created").val(new Date());
        }
    },

    addPaymentExample: function(formObj) {
        $.ajax({
            url: paymentStats.url,
            type: "POST",
            data: $("form").serialize(), // get the form data
            dataType: 'json',
            success: function() {
                paymentStats.showFormSubmissionResponse("Form submission successful");
            },
            error: function() {
                paymentStats.showFormSubmissionResponse("Form submission unsuccessful");
            }
        });

        return false;               
    },

    populateTable: function(data, tableName) {

        $("#payment-container").show();
        $("#payment-details").show();

        if(tableName === "#payments-table") {
            $('#payments-table-filter').hide();
        } else {
            $('#payments-table').hide();
        }


        $("form").hide(); 
        $("#form-response").empty();       

        $(tableName).bootstrapTable("destroy");
        $(tableName).bootstrapTable({
            data: data
        });   
    },

    showFormSubmissionResponse: function(resp) {
        $("form").hide();
        $("#form-response").show();
        $("#form-response").html(resp);
    }

};

$(function () {

    $("#callback").click(function () {
        paymentStats.callbackExample();
    });

    $("#promise").click(function () {        
        paymentStats.promiseExample();        
    });  

    $("#filter").click(function () {    
        paymentStats.filterExample();  
    });  

    $("#add-payment").click(function () {   
        paymentStats.showAddPaymentForm();
    });     

    // validate form
    $("form").validate({
        rules: {
            amount: {
                required: true,
                number: true
            },
            merchant: {
                required: true
            }
        },
        highlight: function (element) {
            $(element).closest('.control-group').removeClass('success').addClass('error');
        },
        success: function (element) {
            element.text('OK!').addClass('valid')
                .closest('.control-group').removeClass('error').addClass('success');
        },
        submitHandler: function(form) {
            // process the form
            paymentStats.addPaymentExample($(this));
        }            
    });                

});